export default [
    {
        title: 'Радионяня Motorola MBP16',
        price: 3690,
        image: '/img/radio.jpg'
    },
    {
        title: 'Ультразвуковая зубная щётка Playbrush Smart Sonic',
        price: 5660,
        image: '/img/toothbrush.jpg'
    },
    {
        title: 'Смартфон Xiaomi Mi Mix 3 6/128GB',
        price: 21790,
        image: '/img/phone.jpg'
    },
    {
        title: 'Электроскейт Razor Cruiser',
        price: 24690,
        image: '/img/board.jpg'
    },
    {
        title: 'Смартфон Xiaomi Mi A3 4/64GB Android One',
        price: 14960,
        image: '/img/phone-2.jpg'
    },
    {
        title: 'Смартфон Xiaomi Redmi 6/128GB',
        price: 8960,
        image: '/img/phone-3.jpg'
    },
    {
        title: 'Электрический дрифт-карт Razor Crazy Cart',
        price: 39900,
        image: '/img/bicycle.jpg'
    },
    {
        title: 'Гироскутер Razor Hovertrax 2.0',
        price: 34900,
        image: '/img/wheels.jpg'
    },
    {
        title: 'Детский трюковой самокат Razor Grom',
        price: 4990,
        image: '/img/scooter.jpg'
    },
    {
        title: 'Роллерсёрф Razor RipStik Air Pro',
        price: 6690,
        image: '/img/ripstik.jpg'
    },
    {
        title: 'Наушники AirPods с беспроводным зарядным футляром',
        price: 16560,
        image: './img/airpods.jpg'
    },
    {
        title: 'Наушники Sony',
        price: 30690,
        image: './img/headphones.jpg'
    },
    {
        title: 'Игровой ноутбук Colorful X15 AT',
        price: 10691,
        image: './img/colorful.webp'
    },
    {
        title: 'Автоматическая кофемашина Delonghi ECAM22.110.SB',
        price: 33249,
        image: './img/delonghi.webp'
    },
    {
        title: 'MacBook',
        price: 33249,
        image: './img/10.webp'
    },
    {
        title: 'Nvidia 4060',
        price: 33249,
        image: './img/11.webp'
    },
    {
        title: 'OnePlus Nord 3',
        price: 33249,
        image: './img/12.webp'
    },
    {
        title: 'Материнская плата',
        price: 33249,
        image: './img/13.webp'
    },
    {
        title: 'Кофемашина Thomson',
        price: 33249,
        image: './img/14.webp'
    },
    {
        title: 'АКофемашина Philips',
        price: 33249,
        image: './img/15.webp'
    },
    {
        title: 'New 5G',
        price: 33249,
        image: './img/16.webp'
    },
    {
        title: 'Фен для волос',
        price: 33249,
        image: './img/17.webp'
    },
    {
        title: 'Системный блок',
        price: 33249,
        image: './img/18.webp'
    },
    {
        title: 'NinKear',
        price: 33249,
        image: './img/19.webp'
    },
    {
        title: 'Стиральная машина CENTEK',
        price: 33249,
        image: './img/20.webp'
    },
    {
        title: 'Утюг Endever',
        price: 33249,
        image: './img/21.webp'
    },
    {
        title: 'Sony playstation 5',
        price: 33249,
        image: './img/22.webp'
    },
    {
        title: 'Чайник',
        price: 33249,
        image: './img/23.webp'
    },
    {
        title: 'Фен Профессиональный',
        price: 33249,
        image: './img/24.webp'
    },
    {
        title: 'Видеокарта Colourful',
        price: 33249,
        image: './img/25.webp'
    },
    {
        title: 'Оперативная память Fury',
        price: 33249,
        image: './img/26.webp'
    },
    {
        title: 'Thunderbot',
        price: 33249,
        image: './img/27.webp'
    },
    {
        title: 'NoutBook V2',
        price: 33249,
        image: './img/6758389302.webp'
    },
]