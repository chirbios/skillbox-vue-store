import Vue from 'vue'
import App from './App.vue'
import { getAlert } from './utils'
import { first, second} from './contains'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

getAlert(first, second);